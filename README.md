#Awesome Immutable Data

# Papers


# Videos

- [Immutable data and React](https://www.youtube.com/watch?v=I7IdS-PbEgI)

# Presentations


# Immutable Data Structures

## Ruby

- [shugo/immutable](https://github.com/shugo/immutable)
- [hamstergem/hamster/](https://github.com/hamstergem/hamster/)

## Javascript

- [facebook/immutable-js](https://github.com/facebook/immutable-js)

# Benchmarks

## Javascript

- [mattbierner/js-hashtrie-benchmark](https://github.com/mattbierner/js-hashtrie-benchmark)
